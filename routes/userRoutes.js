const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth.js")

// Route for user registration
router.post("/register", userController.registerUser);

// Route for user authentication
router.post("/login", userController.loginUser);

// Route for checkout of non-admin user (Create order)
router.post("/orders", auth.verify, userController.order);

// Route for retrieving user details
router.get("/details", auth.verify, userController.getProfile)

module.exports = router;
