const express = require("express");
const mongoose = require("mongoose");

//Allows our backend application to be available to our frontend
const cors = require("cors")

//Allow access to routes defined within the application
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")

const app = express();

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.zmpg8k6.mongodb.net/capstone_ecommerce_api?retryWrites=true&w=majority", 
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }   
);

// Set notifications for database connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We are connected to cloud database"));


//Middlewares

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Middleware that defines the "/users" to be included for all user routes
app.use("/users", userRoutes);
// Middleware that defines the "/products" to be included for all product routes
app.use("/products", productRoutes);


app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
})
