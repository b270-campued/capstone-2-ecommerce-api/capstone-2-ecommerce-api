const Product = require("../models/Product");
const auth = require("../auth.js")

module.exports.addProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin) {

		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price
		});

		return newProduct.save().then(product => {
			console.log(product);
			res.send(true);
		})
	} else {
		return res.send(false)
	}
}

// Retrieve all active products 

module.exports.getAllActiveProduct = (req, res) => {

	return Product.find({isActive: true}).then(result => res.send(result));
}

// Retrieving a specific product

module.exports.getProduct = (req, res) => {

	console.log(req.params.productId);
	return Product.findById(req.params.productId).then(result => {
		return res.send(result)
	})
	.catch(error => {
		return res.send(error);
	})

}

// Update a product information

module.exports.updateProductInfo = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {

		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true}).then(result => {
			console.log(result);
			res.send(result)
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	}
}

//Archive a product


module.exports.archiveProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {

		let archiveProduct = {
			isActive: req.body.isActive
		}

		return Product.findByIdAndUpdate(req.params.productId, archiveProduct, {new:true}).then(result => {
			console.log(result);
			res.send(true)
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
			return res.send(false);
	}
}
