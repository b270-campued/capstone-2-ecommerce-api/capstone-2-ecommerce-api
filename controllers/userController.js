const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")

// User registration
module.exports.registerUser = (req, res) => {
	let newUser = new User({
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	// Save the created object to our database
	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}

// User authentication
module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {

		if(result == null) {
			return res.send({message: "No user found"})

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if (isPasswordCorrect) {

				return res.send({accessToken: auth.createAccessToken(result)});
			
			} else {
				return res.send(false)
			}
		}
	})
}

// Create order of non-admin user
module.exports.order = async (req, res) => {

	//Get the token in header and decode
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false) {

		//This will retrieve the productName in product list using the
		// productId as reference from the request body
		let productName = await Product.findById(req.body.productId).then(result => result.name); 

		let data = {

			// User ID and email will be retrieved from the payload/token
			userId: userData.id,
			email: userData.email,
			// Product ID, quantity, totalAmount will be retrieved from the request body
			productId: req.body.productId,
			quantity: req.body.quantity,
			totalAmount: req.body.totalAmount,
			// Product Name will be from the value of var productName
			productName: productName
		}

		console.log(data);


		let isUserUpdated = await User.findById(data.userId).then(user => {

			console.log(user);

			user.orders.push({
				products: [
					{
						productName: data.productName,
						quantity: data.quantity	
					}
				],
				totalAmount: data.totalAmount
			});

			// Saves the updated user information in the database
			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isUserUpdated);


		// Retrieve orderId in user's order array 
		let orderSavedToUser  = await User.findById(data.userId).then(result => result.orders); 
		console.log(orderSavedToUser);

		let lastElementIndex = orderSavedToUser.length-1;
		let orderIdData = orderSavedToUser[lastElementIndex]._id

		console.log(orderIdData);

		let isProductUpdated = await Product.findById(data.productId)
		.then(product => {

			// Adds the orderId in the course's enrollees array
			product.orders.push({
				orderId: orderIdData.toString()
			})

			// Saves the updated product information in the database
			return product.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isProductUpdated);


		// Condition that will check if the user and course documents have been updated

		// User order successful
		if(isUserUpdated && isProductUpdated) {
			return res.send("Order successful!");

		// User order failed
		} else {
			return res.send("Order failed!");
		}
	} else {
		return res.send("You don't have permission to process order");
	}
}




/*

module.exports.order = async (req, res) => {

	//Get the token in header and decode
	const userData = auth.decode(req.headers.authorization);

	console.log(userData.isAdmin);

	if(userData.isAdmin === false) {

		//This will retrieve the productName in product list using the
		// productId as reference from the request body
		let productName = await Product.findById(req.body.productId).then(result => result.name); 

		let data = {

			// User ID and email will be retrieved from the payload/token
			userId: userData.id,
			email: userData.email,
			// Product ID, quantity, totalAmount will be retrieved from the request body
			productId: req.body.productId,
			quantity: req.body.quantity,
			totalAmount: req.body.totalAmount,
			// Product Name will be from the value of var productName
			productName: productName
		}

		console.log(data);

		let isUserAndProductUpdated = await User.findById(data.userId).then(user => {

			console.log(user);

			// Adds the Product Name, quantity, and totalAmount in the orders array
			user.orders.push({
				products: [
					{
						productName: data.productName,
						quantity: data.quantity	
					}
				],
				totalAmount: data.totalAmount
			});

			// Saves the updated user information in the database
			user.save().then(orderSaveToUser => {
				console.log(orderSaveToUser)
			})


			// Add the order ID in the orders array of the product
			let lastElementIndex = user.orders.length-1;
			let orderIdData = user.orders[lastElementIndex]._id

			console.log(orderIdData);
			
			let isProductUpdated = Product.findById(data.productId)
			.then(product => {

				// Adds the orderID in the products orders array
				product.orders.push({
					orderId: orderIdData.toString()
				})
			

				// Saves the updated product information in the database
				return product.save().then(result => {
					console.log(result);
					return true;
				})
				.catch(error => {
						console.log(error);
						return false;
				});
			});

		})

		console.log(isUserAndProductUpdated);

		// Condition that will check if the user and course documents have been updated

		// User order successful
		if(isUserAndProductUpdated) {
			return res.send("Order successful!");

		// User order failure
		} else {
			return res.send("Order failed!");
		}


	} else {
		return res.send("You are not allowed process the order");
	}

}

*/

/*

module.exports.order = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(!userData.isAdmin) {

		let productName = await Product.findById(req.body.productId)
		.then(result => result.name); //This will retrieve the name of product

		let data = {

			// User ID and email will be retrieved from the payload/token
			userId: userData.id,
			email: userData.email,
			// Product ID, quantity, totalAmount will be retrieved from the request body
			productId: req.body.productId,
			quantity: req.body.quantity,
			totalAmount: req.body.totalAmount,
			// Product Name will be from the value of var productName
			productName: productName
		}
		console.log(data);

		// Add the productName, quantity, and totalAmount in the orders array of the user
		// Add the oderId in the orders array of product

		let isUserAndProductUpdated = await User.findById(data.userId).then(user => {

			console.log(user);

			// Adds the Product Name, quantity, and totalAmount in the orders array
			user.orders.push({
				products: [
					{
						productName: data.productName,
						quantity: data.quantity	
					}
				],
				totalAmount: data.totalAmount
			});

			// Saves the updated user information in the database
			return user.save().then(result => {
				console.log(result);

				// Add the order ID in the orders array of the product
				let lastElementIndex = result.orders.length-1;
				let orderIdData = result.orders[lastElementIndex]._id

				console.log(orderIdData);
				
				let isProductUpdated = Product.findById(data.productId)
				.then(product => {

					// Adds the orderID in the products orders array
					product.orders.push({
						orderId: orderIdData.toString()
					})
				

					// Saves the updated product information in the database
					return product.save().then(result => {
						console.log(result);
						return true;
					})
				});
			})
		})
		.catch(error => {
				console.log(error);
				return false;
		});

		console.log(isUserAndProductUpdated);

		// Condition that will check if the user and course documents have been updated

		// User order successful
		if(isUserAndProductUpdated) {
			return res.send(true);

		// User order failure
		} else {
			return res.send(false);
		}

	} else {
		return res.send(false);
	}
}

*/


// Retrieve user details
module.exports.getProfile = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {

		result.password = "";

		return res.send(result);

	})

}

