const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"],
		unique: true
	},
	password: {
		type: String,
		required: [true, "Password  is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"],
		unique: true
	},
	orders: [

		{
			products: [
			    {
			    	productName: {
			    		type: String
			    	},
				      	quantity: {
				        type: Number
			      	}
			    }
			],
			totalAmount: {
				type: Number
			},
			PurchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	],
	createdOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("User", userSchema);