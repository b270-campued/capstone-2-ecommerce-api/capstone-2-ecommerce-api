const jwt = require("jsonwebtoken")

const secret = "KicksOnFire"

// Token creation
module.exports.createAccessToken = (user) => {

	// The data will be received from the registration form (payload)
	// When the user logs in, a token will be created with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	
	return jwt.sign(data, secret, {});
}

// Token verification

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization 

	
	if (token !== undefined) {
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			// If JWT is not valid
			if (err) {
				return res.send({auth: "failed"});

			} else {

				next();
			}
		})

	} else {

		return res.send({auth: "failed"});
	}
}

// Token Decryption

module.exports.decode = (token) => {
	console.log(token);

	if (token !== undefined) {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if (err) {
				return null;

			} else {
				return jwt.decode(token, {complete:true}).payload;
			}

		})
	} else {

		return null;
	}
}
